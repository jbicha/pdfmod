<?xml version="1.0" encoding="utf-8"?>
<?db.chunk.max_depth 1?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book id="pdfmod" lang="da">
	<bookinfo>
		<title>Manualen til PDF Moderator</title>
		<abstract role="description">
			<para>Dette er brugermanualen til PDF Mod, et enkelt værktøj til manipulering af PDF-dokumenter.</para>
		</abstract>
		<subtitle>Hurtig, nem PDF-redigering</subtitle>
		<copyright>
			<year>2009</year>
			<holder>Novell, Inc.</holder>
		</copyright><copyright><year>2010</year><holder>Joe Hansen</holder></copyright>
		<publisher>
			<publishername>GNOME-dokumentationsprojektet</publishername>
		</publisher>
		<authorgroup>
			<author role="maintainer">
				<firstname>Gabriel</firstname>
				<surname>Burt</surname>
				<affiliation>
					<orgname>Novell, Inc.</orgname>
				</affiliation>
			</author>
		</authorgroup>
		<revhistory>
			<revision>
				<revnumber>0.2</revnumber>
				<date>29-07-2009</date>
			</revision>
			<revision>
				<revnumber>0.1</revnumber>
				<date>25-07-2009</date>
			</revision>
		 </revhistory>
	</bookinfo>

	<chapter id="introduction">
		<title>Indledning</title>
			<para>PDF Moderator er et enkelt værktøj til grundlæggende manipulering af PDF-dokumenter.</para>

			<para>Med dette værktøj kan du rotere, fjerne eller udtrække sider i et PDF-dokument. Du kan også tilføje sider fra et andet dokument.</para>

			<para>Du kan starte PDF Moderator fra din programmenu eller programstarter, eller ved at højreklikke på et eller flere PDF-dokumenter i filbrowseren, og vælge at åbne med PDF Moderator.</para>

			<para>Som med et normalt program der bruges til redigering, skal du gemme ændringerne, du foretager med PDF Moderator. Hvis du ønsker at gemme det oprindelige dokument og gemme dine ændringer i en ny fil så brug Gem som.</para>
	</chapter>

	<chapter id="usage">
		<title>Brug</title>

		<sect1 id="opening-file">
			<title>Åbning af et dokument</title>
			<para>For at åbne et dokument i PDF Moderator:</para>
			<itemizedlist>
			  <listitem>
				  <para>Vælg <menuchoice><guimenu>Fil</guimenu><guimenuitem>Åbn</guimenuitem></menuchoice> og vælg dit dokument, eller</para>
			  </listitem>
			  <listitem>
				  <para>Tryk <keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> og vælg dit dokument, eller</para>
			  </listitem>
			  <listitem>
				  <para>Træk et PDF-dokument fra dit skrivebord eller filbrowser til et vindue i PDF Moderator som ikke allerede har et dokument indlæst, eller</para>
			  </listitem>
			  <listitem>
				  <para>Vælg et dokument fra <menuchoice><guimenu>Fil</guimenu><guimenuitem>Seneste dokumenter</guimenuitem></menuchoice>.</para>
			  </listitem>
		  </itemizedlist>
		</sect1>

		<sect1 id="zooming">
			<title>Zoom</title>
			<para>Du kan zoome for at gøre miniaturebillederne for siderne større eller mindre. PDF Moderator starter i tilstanden bedste tilpasning, hvor programmet forsøger at gøre alle sider synlige på en gang.</para>
			<para>Du kan zoome ind og ud med indstillingerne under <menuchoice><guimenu>Vis</guimenu></menuchoice>, eller ved at trykke på <keycap>Ctrl</keycap> og bevæge din mus' rullehjul.</para>
		</sect1>

		<sect1 id="properties">
			<title>Egenskaber for visning og redigering</title>
			<para>Du kan vise og redigere titel, forfatter, nøgleord og emne i dokumentet ved at åbne egenskaberne. For at gøre dette vælges <menuchoice><guimenu>Fil</guimenu><guimenuitem>Egenskaber</guimenuitem></menuchoice>, tryk <keycombo><keycap>Alt</keycap><keycap>Retur</keycap></keycombo>, eller klik på knappen for egenskaber på værktøjslinjen.</para>
		</sect1>

		<sect1 id="selection">
			<title>Vælge sider</title>
			<para>PDF Moderator kan automatisk vælge alle sider, sider med lige numre, sider med ulige numre eller sider som indeholder en søgeterm. Disse indstillinger er tilgængelige under <menuchoice><guimenu>Rediger</guimenu></menuchoice>.</para>
			<para>Du kan vælge sider manuelt ved at bruge tastaturet og musen. Brug <keycap>Ctrl</keycap> eller <keycap>Skift</keycap> for at vælge mere end en side.</para>
		</sect1>

		<sect1 id="moving-pages">
			<title>Flytte sider</title>
			<para>For at flytte (det vil sige omorganisere) den valgte side eller sider, trækkes de til den nye placering i dokumentet.</para>
			<tip>
				<para>Alle redigeringshandlinger med undtagelse af fjernelse af sider kan fortrydes ved at vælge <menuchoice><guimenu>Rediger</guimenu><guimenuitem>Fortryd</guimenuitem></menuchoice> eller ved at trykke <keycombo><keycap>Ctrl</keycap><keycap>Z</keycap></keycombo>.</para>
			</tip> 
		</sect1>

		<sect1 id="extracting-pages">
			<title>Udtræk af sider</title>
			<para>Udtræk af den valgte side eller sider vil åbne et nyt vindue for PDF Moderator med kun de valgte sider i et nyt dokument, klar til yderligere redigering eller gemning.</para>
			<para>For at udtrække den valgte side eller sider, vælg <menuchoice><guimenu>Rediger</guimenu><guimenuitem>Udtræk side</guimenuitem></menuchoice>.</para>
			<tip>
				<para>Alle redigerings- og markeringshandlingerne der er tilgængelige under <menuchoice><guimenu>Rediger</guimenu></menuchoice> er også tilgængelige ved at højreklikke på en side. Nogle handlinger er også tilgængelige på værktøjslinjen.</para>
			</tip> 
		</sect1>

		<sect1 id="rotating-pages">
			<title>Rotere sider</title>
			<para>For at rotere den valgte side eller sider vælges <menuchoice><guimenu>Rediger</guimenu><guimenuitem>Roter side</guimenuitem></menuchoice> eller tryk <keycombo><keycap>[</keycap></keycombo> for at rotere til venstre (mod uret) og <keycombo><keycap>]</keycap></keycombo> for at rotere til højre (med uret).</para>
		</sect1>

		<sect1 id="removing-pages">
			<title>Fjerne sider</title>
			<para>For at fjerne den valgte side eller sider, tryk <keycap>Slet</keycap> eller vælg <menuchoice><guimenu>Rediger</guimenu><guimenuitem>Fjern side</guimenuitem></menuchoice>.</para>
			<warning>
				<para>Det er i øjeblikket ikke muligt at fortryde denne handling via <menuchoice><guimenu>Rediger</guimenu><guimenuitem>Fortryd</guimenuitem></menuchoice>. Du kan lukke dokumentet uden at gemme og åbne det igen for at få din side tilbage; du vil dog miste alle andre ændringer, du har foretaget.</para>
			</warning> 
		</sect1>

		<sect1 id="saving">
			<title>Gemme</title>
			<para>Efter at du har lavet ændringer til dokumentet, er der to måder du kan gemme dit arbejde. Du kan overskrive det oprindelige dokument ved at vælge <menuchoice><guimenu>Fil</guimenu><guimenuitem>Gem</guimenuitem></menuchoice>, eller du kan gemme dine ændringer til en ny fil ved at vælge <menuchoice><guimenu>Fil</guimenu><guimenuitem>Gem som</guimenuitem></menuchoice>.</para>
		</sect1>

	</chapter>
	
</book>
