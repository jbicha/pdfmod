<?xml version="1.0" encoding="utf-8"?>
<?db.chunk.max_depth 1?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book id="pdfmod" lang="es">
	<bookinfo>
		<title>El manual de PDF Mod</title>
		<abstract role="description">
			<para>Éste es el manual de PDF Mod, una herramienta simple para manipular documentos PDF.</para>
		</abstract>
		<subtitle>Rápida y sencilla modificación de PDF</subtitle>
		<copyright>
			<year>2009</year>
			<holder>Novell, Inc.</holder>
		</copyright><copyright><year>2009</year><holder>Andreu Correa Casablanca (castarco@gmail.com)</holder></copyright>
		<publisher>
			<publishername>Proyecto de documentación GNOME</publishername>
		</publisher>
		<authorgroup>
			<author role="maintainer">
				<firstname>Gabriel</firstname>
				<surname>Burt</surname>
				<affiliation>
					<orgname>Novell, Inc.</orgname>
				</affiliation>
			</author>
		</authorgroup>
		<revhistory>
			<revision>
				<revnumber>0.2</revnumber>
				<date>2009-07-29</date>
			</revision>
			<revision>
				<revnumber>0.1</revnumber>
				<date>2009-07-25</date>
			</revision>
		 </revhistory>
	</bookinfo>

	<chapter id="introduction">
		<title>Introducción</title>
			<para>PDF Mod es una herramienta simple para realizar manipulaciones básicas en documentos PDF.</para>

			<para>Con esta herramienta puede rotar, quitar o extraer páginas de documentos PDF. También puede añadir páginas de otro documento.</para>

			<para>Puede iniciar PDF Mod desde el menú de aplicaciones o el lanzador, o pulsando con el botón derecho del ratón sobre uno o más documentos PDF en el examinador de archivos y escoger abrirlo con PDF Mod.</para>

			<para>Como en una aplicación normal de edición, debe guardar los cambios que vaya realizando con PDF Mod. Si quiere mantener el documento original y guarda sus cambios en un nuevo archivo, usa la opción Guardar como.</para>
	</chapter>

	<chapter id="usage">
		<title>Uso</title>

		<sect1 id="opening-file">
			<title>Abrir un documento</title>
			<para>Para abrir un documento en PDF Mod:</para>
			<itemizedlist>
			  <listitem>
				  <para>Elija <menuchoice><guimenu>Archivo</guimenu><guimenuitem>Abrir</guimenuitem></menuchoice> y seleccione su documento, o</para>
			  </listitem>
			  <listitem>
				  <para>Pulse <keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> y seleccione su documento, o</para>
			  </listitem>
			  <listitem>
				  <para>Arrastre un documento PDF desde su Escritorio o examinador de archivos en una ventana de PDF Mod que todavía no contenga un documento cargado, o</para>
			  </listitem>
			  <listitem>
				  <para>Elija un documento desde <menuchoice><guimenu>Archivo</guimenu><guimenuitem>Documentos recientes</guimenuitem></menuchoice>.</para>
			  </listitem>
		  </itemizedlist>
		</sect1>

		<sect1 id="zooming">
			<title>Ampliación</title>
			<para>La ampliación hará las miniaturas de las páginas más grandes o más pequeñas. PDF Mod se inicia con el modo de mejor ajuste, en el que trata de mostrar todas las páginas a la vez.</para>
			<para>Puede ampliar o reducir la vista con las opciones que hay en <menuchoice><guimenu>Ver</guimenu></menuchoice>, o pulsando <keycap>Ctrl</keycap> y moviendo la rueda de desplazamiento del ratón.</para>
		</sect1>

		<sect1 id="properties">
			<title>Ver y editar propiedades</title>
			<para>Puede ver y editar el título, autor, palabras clave y género del documento abriendo sus propiedades. Para hacerlo, elija <menuchoice><guimenu>Archivo</guimenu><guimenuitem>Propiedades</guimenuitem></menuchoice> pulse, <keycombo><keycap>Alt</keycap><keycap>Intro</keycap></keycombo> o pulse en el botón Propiedades de la barra de herramientas.</para>
		</sect1>

		<sect1 id="selection">
			<title>Seleccionar páginas</title>
			<para>PDF Mod puede seleccionar automáticamente todas las páginas, las páginas pares, las páginas impares o incluso páginas que incluyan un término concreto de búsqueda. Estas opciones están disponibles en <menuchoice><guimenu>Editar</guimenu></menuchoice>.</para>
			<para>Puede seleccionar páginas manualmente usando el teclado y el ratón. Use <keycap>Ctrl</keycap> o <keycap>Mayús</keycap> para seleccionar más de una página.</para>
		</sect1>

		<sect1 id="moving-pages">
			<title>Mover páginas</title>
			<para>Para mover la (o las) páginas seleccionadas, arrástrelas desde donde están a la posición del documento en la que quiere que estén.</para>
			<tip>
				<para>Todas las opciones de edición excepto la eliminación de páginas pueden deshacerse desde el menú <menuchoice><guimenu>Editar</guimenu><guimenuitem>Deshacer</guimenuitem></menuchoice> o pulsando <keycombo><keycap>Ctrl</keycap><keycap>Z</keycap></keycombo>.</para>
			</tip> 
		</sect1>

		<sect1 id="extracting-pages">
			<title>Extraer páginas</title>
			<para>Extraer la (o las) páginas seleccionadas abrirá una nueva ventana de PDF Mod sólo con las páginas seleccionadas en un nuevo documento, a punto para ser editado o guardado.</para>
			<para>Para extraer la (o las) páginas seleccionadas, elija <menuchoice><guimenu>Editar</guimenu><guimenuitem>Extraer página</guimenuitem></menuchoice>.</para>
			<tip>
				<para>Todas las opciones de edición y selección están disponibles en <menuchoice><guimenu>Editar</guimenu></menuchoice> también están disponibles pulsando con el botón derecho del ratón sobre una página. Algunas acciones también están disponibles en la barra de herramientas.</para>
			</tip> 
		</sect1>

		<sect1 id="rotating-pages">
			<title>Rotar páginas</title>
			<para>Para rotar la (o las) páginas seleccionadas, elija <menuchoice><guimenu>Editar</guimenu><guimenuitem>Rotar página</guimenuitem></menuchoice> o pulse <keycombo><keycap>[</keycap></keycombo> para rotar a la izquierda y <keycombo><keycap>]</keycap></keycombo> para rotar a la derecha.</para>
		</sect1>

		<sect1 id="removing-pages">
			<title>Quitar páginas</title>
			<para>Para quitar la (o las) páginas seleccionadas, pulse <keycap>Supr</keycap> o elija <menuchoice><guimenu>Editar</guimenu><guimenuitem>Quitar página</guimenuitem></menuchoice>.</para>
			<warning>
				<para>Actualmente no es posible deshacer esta acción mediante <menuchoice><guimenu>Editar</guimenu><guimenuitem>Deshacer</guimenuitem></menuchoice>. Puede cerrar el documento sin guardar y abrirlo otra vez para recuperar el estado anterior de la página, aunque perderá el resto de cambios que no hayas guardad.</para>
			</warning> 
		</sect1>

		<sect1 id="saving">
			<title>Guardar</title>
			<para>Después de realizar cambios en el documento, hay dos formas diferentes de guardar su trabajo. Puede sobreescribir el documento original eligiendo <menuchoice><guimenu>Archivo</guimenu><guimenuitem>Guardar</guimenuitem></menuchoice> o puede guardar los cambios en un nuevo archivo eligiendo <menuchoice><guimenu>Archivo</guimenu><guimenuitem>Guardar como</guimenuitem></menuchoice>.</para>
		</sect1>

	</chapter>
	
</book>
